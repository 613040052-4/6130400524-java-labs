package sornwieng.porameds.lab10;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV8 extends MobileDeviceFormV7 {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JMenuItem customItem;

	public MobileDeviceFormV8(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public void addMnemonic() {
			fileMenu.setMnemonic(KeyEvent.VK_F);
			newMI.setMnemonic(KeyEvent.VK_N);
			openMI.setMnemonic(KeyEvent.VK_O);
			saveMI.setMnemonic(KeyEvent.VK_S);
			exitMI.setMnemonic(KeyEvent.VK_X);
			configMenu.setMnemonic(KeyEvent.VK_C);
			colorMI.setMnemonic(KeyEvent.VK_L);
		blueMI.setMnemonic(KeyEvent.VK_B);
		greenMI.setMnemonic(KeyEvent.VK_G);
		redMI.setMnemonic(KeyEvent.VK_R);
		customItem.setMnemonic(KeyEvent.VK_U);
	}

	public void addAccelerator() {
		newMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		openMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		saveMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		exitMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));
		blueMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.CTRL_MASK));
		greenMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK));
		redMI.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		customItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.CTRL_MASK));
	}

	public void addComponents() {
		super.addComponents();
		customItem = new JMenuItem("Custom...");

	}

	public void addMenus() {
		super.addMenus();
		colorMenu.add(customItem);
		addMnemonic();
		addAccelerator();
	}

	public static void createAndShowGUI() {
		MobileDeviceFormV8 mobileDeviceFormV8 = new MobileDeviceFormV8("Mobile Device Form V8");

		mobileDeviceFormV8.addComponents();
		mobileDeviceFormV8.getPreferredSize();
		mobileDeviceFormV8.addMenus();
		mobileDeviceFormV8.setFrameFeatures();
		mobileDeviceFormV8.addListeners();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

}
