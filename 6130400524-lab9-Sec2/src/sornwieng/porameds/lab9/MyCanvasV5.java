package sornwieng.porameds.lab9;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

public class MyCanvasV5 extends MyCanvasV4 implements Runnable{
	protected MyBallV2 ball = new MyBallV2(0, ((MyCanvas.HEIGHT/2) - (MyBall.DIAMETER/2)));
	protected Thread running = new Thread(this);
	public MyCanvasV5() {
		super();
		ball.ballVelX = 2;
		ball.ballVelY = 0;
		running.start();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		 int w = WIDTH / 2;
		int h = HEIGHT / 2;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
		//Draw a ball
		g2d.setColor(Color.white);
		g2d.fill(ball);
		
	}


	public void run() {
		while(true) {
			//Stop the ball when hit the right side of the wall
			if(ball.x + MyBall.DIAMETER >= MyCanvas.WIDTH) {
				break;
			}
			//Move the ball
			ball.move();
			
			repaint();
			//Delay
			try
			{
				Thread.sleep(10);
			}
			catch(InterruptedException ex)
			{
				
			}
			
		}
		
	}
}