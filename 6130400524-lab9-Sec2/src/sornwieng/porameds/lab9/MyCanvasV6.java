package sornwieng.porameds.lab9;

import java.awt.geom.Rectangle2D;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;


public class MyCanvasV6 extends MyCanvasV5 implements Runnable{
	protected MyBallV2 ball2 = new MyBallV2(WIDTH/2, HEIGHT/2);
	protected Thread running2 = new Thread(this);
	public MyCanvasV6() {
		super();
		ball2.ballVelX = 1;
		ball2.ballVelY = 1;
		running2.start();
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		int w = WIDTH / 2;
		int h = HEIGHT / 2;
		g2d.setColor(Color.BLACK);
		g2d.fill(new Rectangle2D.Double(0, 0, super.WIDTH, super.HEIGHT));
		//Draw a ball
		g2d.setColor(Color.white);
		g2d.fill(ball2);
		
	}
	public void run() {
		while(true) {
			//Stop the ball when hit the right side of the wall
			if(ball2.x + MyBall.DIAMETER >= MyCanvas.WIDTH || ball2.x <= 0) {
				ball2.ballVelX *= -1;
			}
			if(ball2.y + MyBall.DIAMETER >= MyCanvas.HEIGHT || ball2.y <= 0) {
				ball2.ballVelY *= -1;
			}
			//Move the ball
			ball2.move();
			
			repaint();
			//Delay
			try
			{
				Thread.sleep(10);
			}
			catch(InterruptedException ex)
			{
				
			}
		}
	}
}