package sornwieng.porameds.lab2;

public class ComputeMoney {
		public static void main(String[] args) {
		if (args.length != 4) {
			System.out.println("ComputeMoney <1,000Baht> <500Baht> <100Baht> <20Baht>");
			System.exit(0);
		}
		
		double oneT = 1000 * Double.parseDouble(args[0]);
		double fiveH = 500 * Double.parseDouble(args[1]);
		double oneH = 100 * Double.parseDouble(args[2]);
		double tw = 20 * Double.parseDouble(args[3]);
		double sum =oneT + fiveH + oneH+ tw;
		System.out.println("Total Money is "+sum);
	}

}

