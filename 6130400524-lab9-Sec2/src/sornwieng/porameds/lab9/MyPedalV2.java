package sornwieng.porameds.lab9;

public class MyPedalV2 extends MyPedal {
	// declare speed pedal
	protected final static int speedPedal = 20;

	public MyPedalV2(int x, int y) {
		super(x, y);
	}
	
	public void moveLeft() {
		if (x - speedPedal < 0) {
			x = 0;
		} else {
			x -= speedPedal;
		}
	}

	public void moveRight() {
		if (speedPedal + x + MyPedalV2.PedalWidth > MyCanvas.WIDTH) {
			x = MyCanvas.WIDTH - MyPedal.PedalWidth;
		} else {
			x += speedPedal;
		}
	}
}