package sornwieng.porameds.lab4;

public interface Moveable {
	public void accelerate();
	public void brake();
	public void setSpeed();

}
