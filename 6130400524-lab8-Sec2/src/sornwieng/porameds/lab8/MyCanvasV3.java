package sornwieng.porameds.lab8;


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

import javax.swing.SwingUtilities;

public class MyCanvasV3 extends MyCanvasV2 {
	protected MyBall ball;
	protected MyBrick brick;
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	@Override
	public void paint(Graphics g) {
		super.paintComponent(g);
		setBackground(Color.BLACK);
		Graphics2D g2d = (Graphics2D) g;
		MyBall ball1 = new MyBall(0, 0);
		MyBall ball2 = new MyBall(WIDTH - 30, 0);
		MyBall ball3 = new MyBall(0, HEIGHT - 30);
		MyBall ball4 = new MyBall(WIDTH - 30, HEIGHT - 30);
		MyBall[] ball = {ball1,ball2,ball3,ball4}; 
		
		g2d.setColor(Color.WHITE);
		g2d.fill(ball[0]);
		g2d.fill(ball[1]);
		g2d.fill(ball[2]);
		g2d.fill(ball[3]);

		for (int i = 0; i < 10; i++) {
			g2d.setStroke(new BasicStroke(10));
			MyBrick brick = new MyBrick(0, (HEIGHT / 2) - 10);
			brick.x = 0 + i * 80;
			g2d.setColor(Color.WHITE);
			g2d.fill(brick);
			g2d.setColor(Color.BLACK);
			g2d.draw(brick);
		
		
		
		}
	}
		public static void createAndShowGUI() {
			MyFrameV3 msw = new MyFrameV3("My Frame V3");
			msw.addComponents();
			msw.setFrameFeatures();
		}

		protected void addComponents() {
			add(new MyCanvasV3());
		}
	}