package sornwieng.porameds.lab9;
import javax.swing.*;

public class MyFrame extends JFrame {
	public MyFrame(String string) {
		super(string);
		
	}
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable(){
			public void run() {
				createAndShowGUI();
			}		
		});
	}
	private static void createAndShowGUI() {
		MyFrame msw = new MyFrame("My Frame");
		msw.addComponents();
		msw.setFrameFeatures();

	}
	protected void addComponents() {
		add(new MyCanvas());
	}
	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}