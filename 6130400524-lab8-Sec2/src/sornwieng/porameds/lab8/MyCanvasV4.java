package sornwieng.porameds.lab8;


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.SwingUtilities;

public class MyCanvasV4 extends MyCanvasV3 {
	protected MyBall ball;
	protected MyBrick brick;
	protected MyPedal pedal;
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	
	public void paint(Graphics g) {
		super.paintComponent(g);
		setBackground(Color.BLACK);
		Graphics2D g2d =(Graphics2D) g;
		pedal = new MyPedal((WIDTH - 100) / 2, HEIGHT - 10);
		ball = new MyBall((WIDTH/2)-14,HEIGHT-40);
		g2d.setColor(Color.GRAY);
		g2d.fill(pedal);
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
		
		Color color[] = {Color.yellow,Color.green,Color.orange,Color.red,Color.cyan,Color.blue,Color.magenta};
		MyBrick brick[] = new MyBrick[10];
		g2d.setStroke(new BasicStroke(3));
		int brickWidth = brick[0].brickWidth;
		int brickHeight = brick[0].brickHeight;
		for (int i = 0;i<10;i++) {
			for (int j = 0; j<7;j++) {
				brick[i] = new MyBrick(i*brickWidth, 60 + j * brickHeight);
				g2d.setColor(color[j]);
				g2d.fill(brick[i]);
				g2d.setColor(Color.black);
				g2d.draw(brick[i]);
				
				
			}
		}
	}		
	public static void createAndShowGUI() {
		MyFrameV4 msw = new MyFrameV4("My Frame V4");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	protected void addComponents() {
		add(new MyCanvasV4());
	}
}
