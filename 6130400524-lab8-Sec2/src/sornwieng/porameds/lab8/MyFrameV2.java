package sornwieng.porameds.lab8;

import javax.swing.SwingUtilities;

public class MyFrameV2 extends MyFrame{
	
	public MyFrameV2(String text) {
		super(text);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
protected void addComponents() {
	add(new MyCanvasV2());
	
}

}
