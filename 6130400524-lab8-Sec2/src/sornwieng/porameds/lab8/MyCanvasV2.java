package sornwieng.porameds.lab8;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.SwingUtilities;

public class MyCanvasV2 extends MyCanvas {
	protected MyBall ball; 
	protected MyBrick brick;
	protected MyPedal pedal;
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	@Override
	public void paint(Graphics g) {
		super.paintComponent(g);
		setBackground(Color.BLACK);

		Graphics2D g2d = (Graphics2D) g;
		ball = new MyBall((WIDTH / 2)-15, (HEIGHT / 2)-15);
		brick = new MyBrick(WIDTH/2-40, 0);
		pedal = new MyPedal((WIDTH - 100) / 2, HEIGHT - 10);
		g2d.setColor(Color.WHITE);
		g2d.fill(ball);
		g2d.fill(brick);
		g2d.fill(pedal);
		
		g2d.drawLine(WIDTH / 2, 0, WIDTH / 2,HEIGHT);
		g2d.drawLine(0, HEIGHT / 2, WIDTH, HEIGHT/2);
	}

	public static void createAndShowGUI() {
		MyFrameV2 msw = new MyFrameV2("My Frame V2");
		msw.addComponents();
		msw.setFrameFeatures();
	}

	protected void addComponents() {
		add(new MyCanvasV2());
	}
}
