
package sornwieng.porameds.lab4;

public class Automobile {
	private int gasoline;
	private int speed;
	private int maxspeed;
	private int acceleration;
	private String model;
	private Color color;
	private static int numberOfAutomobile=0;
	
	public enum Color {RED, ORANGE, YELLOW, GREEN, BLUE,INDIGO, VIOLET, WHITE,BLACK}

	public Automobile(int gasoline, int speed, int maxspeed, int acceleration, String string, Color white) {
		this.gasoline = gasoline;
		this.speed = speed;
		this.maxspeed = maxspeed;
		this.acceleration = acceleration;
		this.model = string;
		this.color = white;
		numberOfAutomobile++;
	}
	public Automobile() {
		this.gasoline=0;
		this.speed=0;
		this.model="Automobile";
		this.color=Color.WHITE;
		numberOfAutomobile++;
	}
	public int getGasoline() {
		return gasoline;
	}
	public void setGasoline(int gasoline) {
		this.gasoline = gasoline;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public void setMaxspeed(int maxspeed) {
		this.maxspeed = maxspeed;
	}
	
	public int getMaxspeed() {
		return maxspeed;
	}

	public int getAcceleration() {
		return acceleration;
	}
	public void setAcceleration(int acceleration) {
		this.acceleration = acceleration;
	}

	public String getModel() {
		return model;
	}

	@Override
	public String toString() {
		return "Automobile [gasoline=" + gasoline + ", speed=" + speed + ", maxspeed=" + maxspeed + ", acceleration="
				+ acceleration + ", model=" + model + ", color=" + color + "]";
	}
	public void setModel(String model) {
		this.model = model;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public static int getNumberOfAutomobile() {
		return numberOfAutomobile;
	}



}
