package sornwieng.porameds.lab10;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV9 extends MobileDeviceFormV8 implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MobileDeviceFormV9(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
	public void addComponents() {
		super.addComponents();

	}
	
	public void addMenus() {
		super.addMenus();
		
	}
	
	public void addListeners() {
		openMI.addActionListener(this);
		saveMI.addActionListener(this);
		exitMI.addActionListener(this);
		
		redMI.addActionListener(this);
		greenMI.addActionListener(this);
		blueMI.addActionListener(this);
		customItem.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);
		
		Object src = event.getSource();
		JFileChooser fileChooser = new JFileChooser ( );

		if (src == openMI) {
			fileChooser.setDialogTitle("Open");
			int openButton = fileChooser.showDialog (null, "Open" );
			
			if (openButton == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile ();
                String fileName = fileChooser.getName (selectedFile);
                JOptionPane.showMessageDialog (null, "Opening file " + fileName);
			}
			else {
                JOptionPane.showMessageDialog (null, "Open command cancelled by user.");
			}
			
		} else if(src == saveMI) {
			fileChooser.setDialogTitle("Save");
			int saveButton = fileChooser.showDialog (null, "Save");
			
				if (saveButton == JFileChooser.APPROVE_OPTION) {
					File selectedFile = fileChooser.getSelectedFile();
					String fileName = fileChooser.getName(selectedFile);
					JOptionPane.showMessageDialog (null, "Saving file " + fileName);
				}
				else {
					JOptionPane.showMessageDialog (null, "Save command cancelled by user.");
				}
				
		} else if(src == customItem) {
			Color newColor = JColorChooser.showDialog(null, "Choose color", getBackground());
				if(newColor != null) {
					reviewTxtArea.setBackground(newColor);
				}
				
		} else if (src == redMI) {
			reviewTxtArea.setBackground(Color.RED);
			
		} else if(src == greenMI) {
			reviewTxtArea.setBackground(Color.GREEN);
			
		}else if(src == blueMI) {
			reviewTxtArea.setBackground(Color.BLUE);
			
		}else if(src == exitMI) {
			System.exit(0);
		}
	}


	public static void createAndShowGUI() {
		MobileDeviceFormV9 mobileDeviceFormV9 = new MobileDeviceFormV9("Mobile Device Form V9");
			
		mobileDeviceFormV9.addComponents();
		mobileDeviceFormV9.getPreferredSize();
		mobileDeviceFormV9.addMenus();
		mobileDeviceFormV9.setFrameFeatures();
		mobileDeviceFormV9.addListeners();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}
