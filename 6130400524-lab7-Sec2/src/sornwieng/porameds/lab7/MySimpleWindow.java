package sornwieng.porameds.lab7;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.JLabel;

public class MySimpleWindow extends JFrame {

	
	protected JButton OkButton;
	protected JButton cancelButton;
	protected JPanel bottonPanel;
	protected JPanel contentPanel;
	protected JPanel centerPanel;
	protected JPanel windowPanel;
	public MySimpleWindow(String title) {
		super(title);
	}

	public static void createAndShowGUI() {
		MySimpleWindow mySimp = new MySimpleWindow("My Simple Window");
		mySimp.addComponents();
		mySimp.setFrameFeatures();
		mySimp.setVisible(true);

	}
protected void setFrameFeatures() {
		this.setLocationRelativeTo(null);
		pack();
		setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	protected void addComponents() {
		OkButton = new JButton("OK");
		cancelButton = new JButton("Cancel");
		bottonPanel = new JPanel();

		contentPanel = new JPanel();
		centerPanel = new JPanel();
		centerPanel.setLayout(new BorderLayout());

		bottonPanel.add(cancelButton);
		bottonPanel.add(OkButton);

		windowPanel = (JPanel) this.getContentPane();
		windowPanel.setLayout(new BorderLayout());
		windowPanel.add(centerPanel, BorderLayout.CENTER);
		windowPanel.add(contentPanel, BorderLayout.NORTH);
		windowPanel.add(bottonPanel, BorderLayout.SOUTH);
	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

}
