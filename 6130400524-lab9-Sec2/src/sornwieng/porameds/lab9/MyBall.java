package sornwieng.porameds.lab9;

import java.awt.geom.Ellipse2D;

public class MyBall extends Ellipse2D.Double {
	protected final static int DIAMETER = 30;

	public MyBall(int x, int y) {
		super(x, y, DIAMETER, DIAMETER);
	}
}