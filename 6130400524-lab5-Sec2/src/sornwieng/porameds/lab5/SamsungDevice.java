package sornwieng.porameds.lab5;

public class SamsungDevice  extends MobileDevice {
	private String modelName;
	private int price,weight;
	private double androidVersion;
	private static String brand = " Samsung ";
	
	
	public void displaytime() {
		System.out.println("Display time in both using a digital format and using an analog watch");
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public double getAndroidVersion() {
		return androidVersion;
	}
	public void setAndroidVersion(double androidVersion) {
		this.androidVersion = androidVersion;
	}
	public static String getBrand() {
		return brand;
	}
	public static void setBrand(String brand) {
		SamsungDevice.brand = brand;
	}
	public SamsungDevice(String modelName, int price, int weight, double androidVersion) {
		super(modelName,"Android",price,weight);
		this.androidVersion=androidVersion;
		this.price = price;
		this.modelName = modelName;
		this.weight = weight;
		this.brand = brand;

	}
	@Override
	public String toString() {
		return "SamsungDevice [modelName=" + modelName + ", price=" + price + ", weight=" + weight + ", androidVersion="
				+ androidVersion + "]";
	}


}