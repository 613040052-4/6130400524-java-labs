package sornwieng.porameds.lab5;

public class MobileDevice {
	private String modelName;
	private String os;
	private int price;
	private int weight;


	@Override
	public String toString() {
		return "MobileDevice [modelName:" + modelName + ", os:" + os + ", price:" + price + ", weight:" + weight + "]";
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public MobileDevice(String modelName, String os, int price, int weight) {
		super();
		this.modelName = modelName;
		this.os = os;
		this.price = price;
		this.weight = weight;
		
		
	
	}
	public MobileDevice(String modelName, String os, int price ) {
		super();
		this.modelName = modelName;
		this.os = os;
		this.price = price;
		
	}



	
}