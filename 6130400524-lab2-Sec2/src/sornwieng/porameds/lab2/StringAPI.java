package sornwieng.porameds.lab2;

public class StringAPI {
		public static void main(String[] args) {
		String schName = args[0];
		
		if (schName.endsWith("University")) {
			System.out.println(schName+" is a university");
		} else if (schName.endsWith("College")) {
			System.out.println(schName+" is a college");
		} else {
			System.out.println(schName+" is a neither a university nor a college");
		}

	}

}
