package sornwieng.porameds.lab3;

import java.util.*; 

public class TypingTest {
	
	public static final double STCerrent = System.currentTimeMillis();
	
	public static void Time_Second() {
		double ansCerrent = System.currentTimeMillis();
		double Ans = (ansCerrent - STCerrent) / 1000;
		System.out.println("Your time is " + Ans);
		
		if (Ans <= 12) {
			System.out.println("You type faster than average person");
		} else {
			System.out.println("You type slower than average person");
		}
		System.exit(0);
	}

	public static void main(String[] args) {
		
		String[] Color = {"RED", "ORANGE", "YELLOW", "GREEN", "BLUE", "INDIGO", "VIOLET"};
		
		StringBuilder sb1 = new StringBuilder("");
		for (int i = 0; i < 8; i++) {
			String Color_random = (Color[new Random().nextInt(Color.length)]);
			sb1.append(Color_random);
			sb1.append(" ");
		
		}
		String New = sb1.substring(0, sb1.length()-1);
		System.out.println(New);
		
		while(true) {
			System.out.print("Type your answer: ");
			Scanner scanner = new Scanner(System.in);
			String Answer = scanner.nextLine().toUpperCase();
			
			if (Answer.equals(New)) 
			{
				Time_Second();
				scanner.close();
			}else {
				continue;
			}
			
		
		}
	}

}
