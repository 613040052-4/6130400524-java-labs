package sornwieng.porameds.lab8;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MyCanvas extends JPanel {
	protected final int WIDTH = 800;
	protected final int HEIGHT = 600;

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}

		});
	}

	public MyCanvas() {
		super();
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		setBackground(Color.BLACK);
	}

	@Override
	public void paint(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;

		g2d.setColor(Color.WHITE);
		g2d.drawOval((WIDTH - 300) / 2, (HEIGHT - 300) / 2, 300, 300);
		g2d.fillOval((WIDTH - 100) / 2, (HEIGHT - 90) / 2, 30, 60);
		g2d.fillOval((WIDTH + 100) / 2 - 30, (HEIGHT - 90) / 2, 30, 60);
		g2d.fillRect((WIDTH / 2) - 50, (HEIGHT / 2) + 80, 100, 10);
	}

	protected static void createAndShowGUI() {
		MyFrame nsw = new MyFrame("My Frame");
		nsw.addComponents();
		nsw.setFrameFeatures();

	}
}
