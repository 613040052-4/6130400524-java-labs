package sornwieng.porameds.lab7;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

public class MobileDeviceFormV4 extends MobileDeviceFormV3{
	public MobileDeviceFormV4(String title) {
		super(title);
		
		
		
	}
protected void updateMenuIcon() {
	newItem.setIcon(new ImageIcon("Image/NEW.jpg"));
}
protected void addSubMenu() {
	colorItem.add(new JMenuItem("Red"));
	colorItem.add(new JMenuItem("Green"));
	colorItem.add(new JMenuItem("Blue"));
	sizeItem.add(new JMenuItem("16"));
	sizeItem.add(new JMenuItem("20"));
	sizeItem.add(new JMenuItem("24"));
}


protected void addMenu() {
	
	super.addMenu();
	updateMenuIcon();
	addSubMenu();
	
	}
public static void createAndShowGUI() {
	MobileDeviceFormV4 mobileDeviceForm4 = new MobileDeviceFormV4("Mobile Device Form V4");
	mobileDeviceForm4.addComponents();
	mobileDeviceForm4.addMenu();
	mobileDeviceForm4.setFrameFeatures();
}
public static void main(String[]args) {
	SwingUtilities.invokeLater(new Runnable()  {
		public void run() {
			createAndShowGUI();
		}
	});
}
}
