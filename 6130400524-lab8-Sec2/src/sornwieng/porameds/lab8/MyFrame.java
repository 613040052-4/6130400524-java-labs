package sornwieng.porameds.lab8;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MyFrame extends JFrame {
	protected static MyFrame msw;
	
	public MyFrame(String string) {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected static void createAndShowGUI() {
		MyFrame nsw = new MyFrame("My Frame");
		msw.addComponents();
		msw.setFrameFeatures();

	}

	protected void addComponents() {
		add(new MyCanvas());
	}

	protected void setFrameFeatures() {
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

